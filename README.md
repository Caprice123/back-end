# BCR API

Di dalam repository ini terdapat implementasi API dari Binar Car Rental.
Tugas kalian disini adalah:
1. Fork repository
2. Tulis unit test di dalam repository ini menggunakan `jest`.
3. Coverage minimal 70%

Good luck!


Link:
- Repo: https://gitlab.com/Caprice123/back-end
- Heroku: https://kelvin-cendra-challenge-8.herokuapp.com/
- Testing local: 

    ![testing local result](./test_result_local.png)

Notes:
Testing CI Gitlab tidak berhasil dikarenakan out of memory heap error.
