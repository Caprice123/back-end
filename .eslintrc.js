module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es2021": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": "latest"
    },
    "rules": {
        "semi": ["off", "always"],
        "quotes": [ "off", "double"],
        "no-unused-vars": "off",
        "no-extra-boolean-cast": "off",
        "no-extra-semi": "off"
    },
    "ignorePatterns": ["*.spec.js", "*.test.js"]
}
