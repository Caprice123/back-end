const { CarController } = require('../../../../../app/controllers')
const { Car, UserCar } = require('../../../../../app/models')
const dayjs = require('dayjs')
const { CarAlreadyRentedError } = require('../../../../../app/errors')

describe('handleRentCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(async () => {
            const userCars = await userCarModel.findAll({
                limit: 1,
                order: [ ['createdAt', 'DESC' ] ]
            })
            
            const userCarId = userCars[0].id
            await userCarModel.destroy({ where: { id: userCarId } })
        })

        it('(valid request params and body) should return status code 201 and return a new rented car details', async () => {
            const newRentDetail = {
                rentStartedAt: new Date()
            }
            const mReq = { body: newRentDetail, params: { id: 1 }, user: { id: 1 } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleRentCar(mReq, mRes, mNext)

            expect(mRes.status).toBeCalledWith(201)
            expect(mRes.json).toBeDefined()
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {

        })

        afterAll(async () => {
            const userCars = await userCarModel.findAll({
                limit: 1,
                order: [ ['createdAt', 'DESC' ] ]
            })
            
            const userCarId = userCars[0].id
            await userCarModel.destroy({ where: { id: userCarId } })
        })

        it('(car already rented) should return 422 and return error CarAlreadyRented', async () => {
            const mReq1 = { body: { rentStartedAt: new Date(new Date().setDate(new Date(). getDate())), rentEndedAt: new Date(new Date().setDate(new Date(). getDate() + 1)) }, params: { id: 1 }, user: { id: 1 } }
            const mRes1 = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext1 = jest.fn()
            await carController.handleRentCar(mReq1, mRes1, mNext1)
            
            
            const mReq2 = mReq1
            const mRes2 = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext2 = jest.fn()
            await carController.handleRentCar(mReq2, mRes2, mNext2)

            const car = await carModel.findByPk(1)
            const expectedError = new CarAlreadyRentedError(car)
            expect(mRes2.status).toBeCalledWith(422)
            expect(mRes2.json).toBeCalledWith(expectedError)
        })

        it('(empty request params) should hit handleError function', async () => {
            const mReq = { body: { rentStartedAt: new Date(new Date().setDate(new Date(). getDate())) }, params: {  }, user: { id: 1 } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleRentCar(mReq, mRes, mNext)

            expect(mNext).toHaveBeenCalled()
        })
        it('(empty request user) should hit handleError function', async () => {
            const mReq = { body: { rentStartedAt: new Date(new Date().setDate(new Date(). getDate())) }, params: { id: 1 }}
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleRentCar(mReq, mRes, mNext)

            expect(mNext).toHaveBeenCalled()
        })
    })
})
