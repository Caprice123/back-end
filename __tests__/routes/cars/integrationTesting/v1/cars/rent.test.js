const app = require('../../../../../../app')
const { UserCar, Car } = require('../../../../../../app/models')
const request = require('supertest')
const { CarAlreadyRentedError, InsufficientAccessError } = require('../../../../../../app/errors')
const { JsonWebTokenError } = require('jsonwebtoken')


describe('Rent function (POST /v1/cars/:id/rent', () => {
    const userCarModel = UserCar
    const carModel = Car
    describe('Successful Operation', () => {
        let token
        const userCredential = {
            email: "brian@binar.co.id",
            password: "123456",
        }

        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    token = res.body.accessToken
                    
                    done()
                })
            })
        
        afterAll(() => {

        })

        it('(valid request params and request body) should be returning status code 201 and description of rented car', (done) => {
            const carRentedDetail = {
                rentStartedAt: new Date()
            }
            const carRentedId = 1
            request(app)
                .post(`/v1/cars/${carRentedId}/rent`)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${token}`)
                .send(carRentedDetail)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    
                    expect(res.body.carId).toEqual(carRentedId)
                    expect(new Date(res.body.rentStartedAt)).toEqual(new Date(carRentedDetail.rentStartedAt))
                    
                    await userCarModel.destroy({ where: { id: res.body.id } })
                    done()
                })
        })
    })    

    describe('Error Operation', () => {
        let validToken
        const validUserCredential = {
            email: "brian@binar.co.id",
            password: "123456"
        }
        
        let invalidToken
        const invalidUserCredential = {
            email: "johnny@binar.co.id",
            password: "123456",
        }

        let car

        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login').
                send(validUserCredential)
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    validToken = res.body.accessToken

                    request(app)
                        .post('/v1/auth/login')
                        .send(invalidUserCredential)
                        .end(async (err, res) => {
                            if (err) return done(err)
        
                            expect(res.header['content-type']).toMatch(/json/)
                            expect(res.status).toEqual(201)
                            invalidToken = res.body.accessToken
                            
                            car = await carModel.findByPk(1)
                            done()
                        })
                })
            })

        afterAll(() => {
            
        })

        it('(car already rented) should return status code 422 and return error CarAlradyRentedError', (done) => {
            const carRentedId = 1
            const carRentedDetail = {
                rentStartedAt: new Date(),
                rentEndedAt: new Date(new Date(). setDate(new Date(). getDate() + 1))
            }
            const expectedError = new CarAlreadyRentedError(car)
            const expectedResponse = {
                error: {
                    details: expectedError.details,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            request(app)
                .post(`/v1/cars/${carRentedId}/rent`)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${validToken}`)
                .send(carRentedDetail)
                .end((err, res) => {
                    if (err) return done(err)
        
                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)

                    let createdRentDetail = res.body
                    request(app)
                        .post(`/v1/cars/${carRentedId}/rent`)
                        .set('Accept', 'application/json')
                        .set('Authorization', `Bearer ${validToken}`)
                        .send(carRentedDetail)
                        .end(async (err, res) => {
                            if (err) return done(err)
                
                            expect(res.header['content-type']).toMatch(/json/)
                            expect(res.status).toEqual(422)

                            expect(res.body).toEqual(expectedResponse)

                            await userCarModel.destroy({ where: { id: createdRentDetail.id } })
                            done()
                        })
                })
        })

        it('(invalid token) should be returning status code 401 and return error InsufficientAccessError', (done) => {
            const expectedError = new InsufficientAccessError("ADMIN")
            const expectedResponse = {
                error: {
                    details: expectedError.details,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            const carRentedDetail = {
                rentStartedAt: new Date()
            }
            const carRentedId = 1
            request(app)
                .post(`/v1/cars/${carRentedId}/rent`)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer ${invalidToken}`)
                .send(carRentedDetail)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)
                    
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })

        it('(no authorization header) should be returning status code 401 and return error JsonWebTokenError', (done) => {
            const expectedError = new JsonWebTokenError("jwt must be provided")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            const carRentedDetail = {
                rentStartedAt: new Date()
            }
            const carRentedId = 1
            request(app)
                .post(`/v1/cars/${carRentedId}/rent`)
                .set('Accept', 'application/json')
                .send(carRentedDetail)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)
                    
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })

        it('(no token) should be returning status code 401 and return error JsonWebTokenError', (done) => {
            const expectedError = new JsonWebTokenError("jwt malformed")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            const carRentedDetail = {
                rentStartedAt: new Date()
            }
            const carRentedId = 1
            request(app)
                .post(`/v1/cars/${carRentedId}/rent`)
                .set('Accept', 'application/json')
                .set('Authorization', `Bearer 123456`)
                .send(carRentedDetail)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)
                    
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })
    })
})