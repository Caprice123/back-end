const { Role, User } = require('../../../../../app/models')
const AuthenticationController = require('../../../../../app/controllers/AuthenticationController')
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");


describe('decodeToken function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    describe('Successful Operation', () => {
        let user
        let userRole
        let payload
        let token
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
            payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image,
                role: {
                  id: userRole.id,
                  name: userRole.name,
                }
            }

            token = authenticationController.createTokenFromUser(user, userRole)
        })

        afterAll(() => {

        })
        it('(valid token) should return the user object from corresponding token', async () => {
            const resultUser = authenticationController.decodeToken(token)
            expect(resultUser.id).toEqual(payload.id)
            expect(resultUser.name).toEqual(payload.name)
            expect(resultUser.email).toEqual(payload.email)
            expect(resultUser.image).toEqual(payload.image)
            expect(resultUser.role.id).toEqual(payload.role.id)
            expect(resultUser.role.name).toEqual(payload.role.name)
        }) 
    })

    describe('Error Operation', () => {
        let user
        let userRole
        let payload
        let token
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
            payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image,
                role: {
                  id: userRole.id,
                  name: userRole.name,
                }
            }

            token = authenticationController.createTokenFromUser(user, userRole)
        })

        afterAll(() => {

        })

        it('(invalid token) should return error with message JsonWebTokenError', async () => {
            const expectedError = new jwt.JsonWebTokenError("invalid signature")
            const func = () => {
                try{
                    authenticationController.decodeToken(token.slice(0, -1))
                } catch(err){
                    throw err
                }
            }
            expect(func).toThrow(expectedError)
        })

        it('(no token) should return error with message JsonWebTokenError', async () => {
            const expectedError = new jwt.JsonWebTokenError("jwt must be provided")
            const func = () => {
                try{
                    authenticationController.decodeToken()
                } catch(err){
                    throw err
                }
            }
            expect(func).toThrow(expectedError)
        })
    })
})
