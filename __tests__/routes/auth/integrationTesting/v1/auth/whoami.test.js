const app = require('../../../../../../app')
const request = require('supertest')
const { InsufficientAccessError } = require('../../../../../../app/errors')
const jwt = require('jsonwebtoken')

describe('Whoami route (GET /v1/auth/whoami', () => {
    describe('Successful Operation', () => {
        let token
        const userCredential = {
            email: "brian@binar.co.id",
            password: "123456",
        }

        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    token = res.body.accessToken
                    
                    done()
                })
            })

        afterAll(() => {

        })

        it('(valid token) should return status code 200 and return user account with the corresponding jwt token', (done) => {
            request(app)
                .get('/v1/auth/whoami')
                .set('Authorization', `Bearer ${token}`)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(200)
                    expect(res.body.email).toEqual(userCredential.email)
                    done()
                })
        })
    })

    describe('Error Operation', () => {
        let invalidToken
        const invalidUserCredential = {
            email: "johnny@binar.co.id",
            password: "123456"
        }
        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login')
                .send(invalidUserCredential)
                .end((err, res) => {
                    if (err) return done(err)
                    
                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    invalidToken = res.body.accessToken
                    done()
                })
        })

        afterAll(() => {

        })
        it('(empty authorization header) should return status code 401 and return error JwonWebTokenError', (done) => {
            const expectedError = new jwt.JsonWebTokenError("jwt must be provided")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .get('/v1/auth/whoami')
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)
                    
                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })

        it('(invalid role) should return status code 401 and return error InsufficientAccessError', (done) => {
            const expectedError = new InsufficientAccessError("ADMIN")
            const expectedResponse = {
                error: {
                    details: expectedError.details,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .get('/v1/auth/whoami')
                .set('Authorization', `Bearer ${invalidToken}`)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })
        
        it('(invalid token) should return status code 401 and return error JsonWebTokenError', (done) => {
            const expectedError = new jwt.JsonWebTokenError("jwt malformed")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .get('/v1/auth/whoami')
                .set('Authorization', `Bearer 123456`)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })
    })
})

